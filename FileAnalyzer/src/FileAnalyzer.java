import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileAnalyzer {

    private final static String PATHFILE1 = "resources/LoremIpsum1";
    private final static String PATHFILE2 = "resources/LoremIpsum2";

    public static void main(String[] args) {

        FileAnalyzer fileAnalyzer = new FileAnalyzer();
        System.out.println(fileAnalyzer.count());
        fileAnalyzer.firstBigWord(11).ifPresent(System.out::println);
        fileAnalyzer.longestWords(5).forEach(System.out::println);
        fileAnalyzer.commonWords().forEach(System.out::println);

    }

    public long count() {
        System.out.println("Number of words in file:");
        return getWords(PATHFILE1)
                .count();
    }

    public Optional<String> firstBigWord(int n) {
        System.out.println("\n" + "First word to appear bigger than " + n + " letters:");

         return getWords(PATHFILE1)
                .filter(l -> l.length() > n)
                .findFirst();
    }

    public Stream<String> longestWords(int n) {
        System.out.println("\n" + "The " + n + " longest words:" );

        return getWords(PATHFILE1)
                .sorted(Comparator.comparingInt(String::length).reversed())
                 .map(String::toLowerCase)
                 .distinct()
                .limit(n);
    }

    public Stream<String> commonWords() {
        System.out.println("\n" + "Common words between two files:");

        return getWords(PATHFILE1)
                .distinct()
                .sorted(Comparator.comparing(w -> getWords(PATHFILE2)
                        .collect(Collectors.toSet())
                        .contains(w)))
                .sorted(Comparator.comparingInt(String::length).reversed());
    }

    public Stream<String> getWords(String path) {
        Stream<String> stringStream = null;
        try {
            stringStream = Files.lines(Paths.get(path));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return Arrays.stream(stringStream
                .collect(Collectors.joining())
                .split("\\W+"));

    }

}
